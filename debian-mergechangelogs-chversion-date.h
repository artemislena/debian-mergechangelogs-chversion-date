#ifndef debian_mergechangelogs_chversion_h_INCLUDED
#define debian_mergechangelogs_chversion_h_INCLUDED 

#define MAX_LENGTH 2048 /*Max length of a single change*/
#define MAX_VERSION_LENGTH 20 /*Max length of a version number*/

/*misc-functions.c*/
void formatError(char[]);
short getDigits(int);
void mvstr(char[], int, int);

/*getsinglelog.c*/
short getSingleLog(char[], FILE *, bool *);

/*getversion.c*/
void getVersion(char[], char[], int[]);

/*bumpversion.c*/
short bumpVersion(char[], int, char[], int[]);

#endif

