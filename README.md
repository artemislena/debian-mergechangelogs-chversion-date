# Debian changelog Git merge driver which updates version and date 

A C program which can be used as a merge driver for Debian changelog files.  

Changelogs in the file of the branch that shall be merged with the current one will have their timestamp set to the current time, their version increased to whatever it should be now, and will simply be appended to the beginning of the file.
Does not check for inconsistencies after the first change that was the same in both files.  

How to install/compile:
Dependencies needed are C standard libraries (stdio.h, stdlib.h, string.h, time.h), as well as a compiler (Clang used here) for compiling. GNU make will be needed for
using the makefile to install. You'll also want Git for actually using it.  

1. Download the repository and change into directory:  
`git clone https://gitlab.com/FantasyCookie17/debian-mergechangelogs-chversion-date/ && cd debian-mergechangelogs-chversion-date/`  

2. Install:  
`make install`  

3. Define it as a merge driver in your `.gitconfig`…  
```
[merge "debian-mergechangelogs-chversion-date"]
name= A C program which can be used as a merge driver for Debian changelog files.
driver = debian-mergechangelogs-chversion-date %A %B %P
```
…and make sure it's used for changelog files in your `.gitattributes`:
```
changelog merge=debian-mergechangelogs-chversion-date
```

## TODO:
- Build proper Debian package with changelog, proper maintainer name and so on, so it can be uploaded to official Debian repos (or at least those of Univention, 
who might be the main users of this software after all) and let makefile build Debian package.
- Fix any bugs that might be encountered, and make sure functionality is as expected (permanent task).
