#include "debian-mergechangelogs-chversion-date.h"

/*Throws an error for a given reason and terminates program*/
void formatError(char reason[]){
    printf("debian-mergechangelogs-chversion-date: Error: %s\n", reason);
    exit(2);
}

/*Returns the number of digits of an integer. Negative integers should not be passed, as their digit count will always be 1.*/
short getDigits(int number){
    short digits;
    for(digits = 1; number >= 10; digits++)
        number = number / 10;
    /*No check needed, as int can never have enough digits to let short overflow*/
    return digits;
}

/*Moves a string for a specified amount of fields. Does not clean up after itself. Due to how arrays work, rather slow.*/
void mvstr(char string[MAX_LENGTH], int start, int amount){
    int length = strlen(string) + 1; /*Include 0 */

    /*Prevent buffer overflow*/
    if(length + amount >= MAX_LENGTH)
        formatError("Log too long after version number increase.");

    for(; length >= start; length--)
        string[length+amount] = string[length];
}
