/*Program to merge Debian changelog files*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>

#include "debian-mergechangelogs-chversion-date.h"

#include "misc-functions.c"
#include "getsinglelog.c"
#include "getversion.c"
#include "bumpversion.c"

int main(int argc, char* argv[]){

    if(argc < 4){
        printf("Not enough arguments.\n");
        printf("Usage: dpkg-merge-correctly %%A %%B %%P\n");
        return 1;
    }

    FILE *old, *new, *output;
    old = fopen(argv[1], "r+"); /*The old file; %A.*/
    new = fopen(argv[2], "r"); /*The file that shall be merged; %B.*/
    output = fopen(argv[3], "w+"); /*The temporary output file; %P.*/

    if(old == NULL || new == NULL || output == NULL){
        printf("Error opening file.\n");
        return -1;
    }

    char oldLog[MAX_LENGTH], newLog[MAX_LENGTH]; /*Strings to hold single logs*/
    bool hitOldLog = false, oldEof = false, newEof = false;
    int newLogs = -1;
    char currentVersion[MAX_VERSION_LENGTH];
    int currentVersionstartdashstop[3];

    /*Get current version*/
    getSingleLog(oldLog, old, &oldEof);
    getVersion(oldLog, currentVersion, currentVersionstartdashstop);
    rewind(old), oldEof = false;

    /*Determine amount of new logs*/
    while(!hitOldLog){
        newLogs++;
        getSingleLog(newLog, new, &newEof);

        if(newLogs < -1)
            formatError("Wow, apparently someone implemented more than INT_MAX changes compared with the branch you tried to merge with.");

        while(!oldEof){
            getSingleLog(oldLog, old, &oldEof);
            if(strcmp(oldLog, newLog) == 0){
                hitOldLog = true;
                break;
                }
        }
        rewind(old), oldEof = false;

        if(newEof && !hitOldLog)
            formatError("Files are not coherent.");
    }
    rewind(new), newEof = false;

    /*Generate current time*/
    const static int dateLength = 32;
    char currentTime[dateLength];
    time_t rawtime;
    time(&rawtime);
    strftime(currentTime, dateLength, "%a, %d %b %Y %H:%M:%S %z", localtime(&rawtime));

    /*Now print the new changes into output file, modifying them before doing so*/
    for(int i = newLogs; i > 0; i--){
        short dc = getSingleLog(newLog, new, &newEof);

        /*Insert new version number and increase position of date accordingly*/
        dc += bumpVersion(newLog, i, currentVersion, currentVersionstartdashstop);

        /*Replace time in changelog with current time*/
        strncpy(&newLog[dc], currentTime, dateLength-1);

        /*Now print the result into file*/
        fprintf(output, "%s", newLog);
    }

    fclose(new);

    /*Print rest of old changelog into output file*/
    for(int c=fgetc(old); c != EOF; c=fgetc(old))
        fputc(c, output);

    /*Now put everything into %A*/
    rewind(output), rewind(old);
    for(int c=fgetc(output); c != EOF; c=fgetc(output)) /*Output is guaranteed to completely overwrite old by now, as it contains all of old and likely more than that*/
        fputc(c, old);

    fclose(old), fclose(output);
    return 0;
}
