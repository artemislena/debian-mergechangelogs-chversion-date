#Usage:
#make			# Build binary in current directory.
#make buildman		# Build the manpage.
#make install		# Same as make and make buildman combined, but move binary to /usr/bin/ and move manpage
			# to /usr/share/man/man1/. Requires sudo.
#make debug		# Build a binary for debugging with .dbg file extension.
#make debpkg		# Build a Debian package. Not implemented currently.
#make installdeb	# Build and install a Debian package. Not implemented currently.
#make clean		# Remove all built files that are not installed.
#make uninstall		# Remove installed binaries and manpages. Requires sudo.

CC = clang -flto -fvisibility=hidden -fsanitize=cfi
options = -O2
debug_options = -O0 -g
source_name = ./debian-mergechangelogs-chversion-date.c

build:
	${CC} ${options} ${source_name} -o debian-mergechangelogs-chversion-date

buildman:
	gzip -k Debian/debian-mergechangelogs-chversion-date.1

install: build buildman
	sudo mv debian-mergechangelogs-chversion-date /usr/bin/
	sudo mv Debian/debian-mergechangelogs-chversion-date.1.gz /usr/share/man/man1/

debug:
	${CC} ${debug_options} ${source_name} -o debian-mergechangelogs-chversion-date.dbg

clean:
	@test -e debian-mergechangelogs-chversion-date && rm debian-mergechangelogs-chversion-date || true
	@test -e *.dbg && rm ./*.dbg || true
	@test -e Debian/debian-mergechangelogs-chversion-date.1.gz && rm Debian/debian-mergechangelogs-chversion-date.1.gz || true

uninstall:
	sudo rm /usr/bin/debian-mergechangelogs-chversion-date || true
	sudo rm /usr/share/man/man1/debian-mergechangelogs-chversion-date.1.gz || true
