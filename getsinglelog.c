#include "debian-mergechangelogs-chversion-date.h"

/*Gets a single change and returns character where date resides*/
short getSingleLog(char log[MAX_LENGTH], FILE *file, bool *Eof){

    bool endOfLog = false, lastLine = false;
    short cc; /*Character count*/
    short dc=0; /*Character of start of date*/
    int c;

    for(cc = 0; !endOfLog; cc++){
        c = fgetc(file);
        
        if(c == EOF){
            *Eof = true;
            break;
        }

        if(cc == MAX_LENGTH) /*Prevent buffer overflow*/
            formatError("Change too long.");

        log[cc] = c;

        if(!lastLine && cc > 5 && strncmp(&log[cc-5], "\n\n -- ", 6) == 0) /*format for last line*/
            lastLine = true;

        if(lastLine){
            if(strncmp(&log[cc-1], "\n\n", 2) == 0) /*End of changelog*/
                endOfLog = true;

            /*Begin of date (check for letter of weekday)*/
            if(dc == 0 && (c == 'M' || c == 'T' || c == 'W' || c == 'F' || c == 'S') && strncmp(&log[cc-3], ">  ", 3) == 0)
                dc=cc;
        }            
    }
    log[cc] = '\0'; /*Terminate string here*/

    if(dc == 0) /*Date was not encountered*/
        formatError("Date or other part of last line not formatted correctly.");
    return dc;
}
