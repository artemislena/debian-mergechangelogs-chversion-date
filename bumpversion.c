#include "debian-mergechangelogs-chversion-date.h"

/*Increases the version number of a Debian changelog by a specified amount. Return number of characters moved.*/
short bumpVersion(char log[MAX_LENGTH], int amount, char currentVersionString[MAX_VERSION_LENGTH], int currentVersionstartdashstop[3]){

    /*Convert version number string to integer.*/
    int currentVersion = atoi(&currentVersionString[currentVersionstartdashstop[1] - currentVersionstartdashstop[0] + 1]);

    char newVersionString[MAX_VERSION_LENGTH];
    int newVersionstartdashstop[3];
    getVersion(log, newVersionString, newVersionstartdashstop);

    int newVersion = currentVersion + amount;
    if(newVersion < 0)
        formatError("New version number reached INT_MAX.");

    /*Check for increases in number of digits and move accordingly*/
    short digitsDifference = getDigits(newVersion) - getDigits(currentVersion);
    if(digitsDifference > 0){
        if(newVersionstartdashstop[1] - newVersionstartdashstop[0] + 1 + digitsDifference >= MAX_VERSION_LENGTH) /*Prevent buffer overflow later*/
           formatError("Version number after increase too long.");
        mvstr(log, newVersionstartdashstop[2] + 1, digitsDifference);
    }

    /*Insert new version number after converting to string*/
    sprintf(&newVersionString[newVersionstartdashstop[1] - newVersionstartdashstop[0] + 1], "%d", newVersion);
    strncpy(&log[newVersionstartdashstop[0]], newVersionString, strlen(newVersionString));
    return digitsDifference;
}
