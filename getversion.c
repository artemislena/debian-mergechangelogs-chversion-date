#include "debian-mergechangelogs-chversion-date.h"

/*Gets version number from specified log and saves in version; saves locations of start, dash and end in startdashstop*/
void getVersion(char log[MAX_LENGTH], char version[MAX_VERSION_LENGTH], int startdashstop[3]){
    bool bracketEncountered=false, dashEncountered = false, stop = false;
    int j = 0;
    for(int i = 0; !stop; i++){

        if(bracketEncountered){
            if(j == MAX_VERSION_LENGTH) /*Prevent buffer overflow*/
                formatError("Version number too long.");
            version[j] = log[i];
            j++;
        }
        
        if(log[i] == '\n')
            formatError("Version number formatted incorrectly.");
        if(log[i] == '('){
            if(bracketEncountered)
                formatError("Version number formatted incorrectly.");
            bracketEncountered = true;
            startdashstop[0] = i+1;
        }
        if(log[i] == '-' && bracketEncountered){
            if(dashEncountered)
                formatError("Version number formatted incorrectly.");
            dashEncountered = true;
            startdashstop[1] = i;
        }
        if(log[i] == ')'){
            if(!dashEncountered)
                formatError("Version number formatted incorrectly.");
            stop = true;
            startdashstop[2] = i-1;
        }
    }
    version[--j] = '\0'; /*Terminate string*/
}        
